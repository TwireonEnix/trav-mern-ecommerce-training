const { listProducts, getProductById } = require('./core/products.crud');

module.exports = {
  async retrieveAllProducts(req, res, next) {
    const data = await listProducts();
    res.send({ code: 200, message: 'Products retrieved', data });
  },

  async retrieveSingleProduct({ params: { id } }, res, next) {
    const data = await getProductById(id);
    if (data.error) return next(data);
    res.send({ code: 200, message: 'Product retrieved', data });
  }
};
