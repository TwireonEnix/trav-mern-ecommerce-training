const R = require('ramda');
const { Product } = require('../product.model');
const { errorResponse } = require('../../../utils');

const listProducts = () => Product.find().lean().exec();

const getProductById = async id => {
  const product = await Product.findById(id).lean().exec();
  return R.when(R.isNil, R.always(errorResponse(404, 'Product not found')), product);
};

module.exports = { listProducts, getProductById };
