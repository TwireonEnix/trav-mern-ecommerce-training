const router = require('express').Router();
const { retrieveAllProducts, retrieveSingleProduct } = require('./controller');
const validId = require('../../middleware/validObjectId');

router.get('/', retrieveAllProducts);
router.route('/:id').all(validId).get(retrieveSingleProduct);

module.exports = router;
