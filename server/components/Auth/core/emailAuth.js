const R = require('ramda');
const { User } = require('../../User/user.model');
const { errorResponse } = require('../../../utils');

const authUserWithEmail = async ({ email, password }) => {
  const user = await User.findOne({ email }).exec();

  if (R.isNil(user)) return errorResponse(404, 'User not found');

  const correctPassword = await user.matchPassword(password);
  if (R.not(correctPassword)) return errorResponse(401, 'Wrong user password');

  return {
    ...R.pick(['_id', 'name', 'email', 'isAdmin'], user.toJSON()),
    token: User.generateJWT(R.pick(['_id', 'isAdmin'], user))
  };
};

const registerUserWithEmail = async userData => {
  const { email, password: plainPassword } = userData;
  const userExist = User.exists({ email });
  if (userExist) return errorResponse(400, 'User already exists');

  const password = User.encryptPassword(plainPassword);
  return User.create({ ...userData, password });
};

module.exports = { authUserWithEmail, registerUserWithEmail };
