const router = require('express').Router();
const { login } = require('./auth.controller');
const { loginValSchema } = require('./common/validation');
const { validateBodyWith } = require('../../middleware/validateWith');

router.post('/login', validateBodyWith(loginValSchema), login);

module.exports = router;
