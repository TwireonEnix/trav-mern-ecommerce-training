const { authUserWithEmail, registerUserWithEmail } = require('./core/emailAuth');

module.exports = {
  async login({ body }, res, next) {
    const data = await authUserWithEmail(body);
    if (data.error) return next(data);
    res.send({ code: 200, message: 'Login successful', data });
  },

  async registerUserWithEmail({ body }, res, next) {
    const data = await registerUserWithEmail(body);
    if (data.error) return next(data);
    res.send({ code: 200, message: 'User registered successfully', data });
  }
};
