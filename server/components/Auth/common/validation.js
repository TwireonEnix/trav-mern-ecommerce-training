const Joi = require('joi');

const loginValSchema = Joi.object({
  email: Joi.string().email().required(),
  password: Joi.string().min(5).required()
});

const registerValSchema = Joi.object({
  name: Joi.string().min(2).required(),
  email: Joi.string().email().required(),
  password: Joi.string().min(5).required()
});

module.exports = { loginValSchema };
