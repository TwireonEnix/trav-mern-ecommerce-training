const bcrypt = require('bcryptjs');
const getHash = () => bcrypt.hashSync('12345', 10);

const users = [
  {
    name: 'Admin User',
    email: 'admin@store.com',
    password: getHash(),
    isAdmin: true
  },
  {
    name: 'John Doe',
    email: 'john@store.com',
    password: getHash(),
    isAdmin: false
  },
  {
    name: 'Jane Doe',
    email: 'jane@store.com',
    password: getHash(),
    isAdmin: false
  }
];

module.exports = { users };
