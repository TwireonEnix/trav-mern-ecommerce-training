const userModel = require('../user.model');
const { User } = require('../user.model');

const getUserProfile = uId => User.findById(uId).select('-password -createdAt -updatedAt -__v').lean().exec();

module.exports = { getUserProfile };
