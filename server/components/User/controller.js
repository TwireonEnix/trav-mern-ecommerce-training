const { getUserProfile } = require('./core/user.crud');

module.exports = {
  async me({ user: { _id } }, res, next) {
    const data = await getUserProfile(_id);
    res.send({ code: 200, message: 'User fetched successfully', data });
  }
};
