const router = require('express').Router();
const { me } = require('./controller');
const verifyJWT = require('../../middleware/verifyAuth');

router.get('/profile', verifyJWT, me);

module.exports = router;
