const mongoose = require('mongoose');

const paymentResultSchema = new mongoose.Schema(
  {
    paymentId: { type: String, required: true },
    status: { type: String, required: true },
    update_time: { type: String, required: true },
    email_address: { type: Number, required: true }
  },
  { _id: false }
);

module.exports = paymentResultSchema;
