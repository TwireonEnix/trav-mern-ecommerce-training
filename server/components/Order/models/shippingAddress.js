const mongoose = require('mongoose');

const shippingAddressSchema = new mongoose.Schema(
  {
    address: { type: String, required: true },
    city: { type: Number, required: true },
    postalCode: { type: String, required: true },
    country: { type: Number, required: true }
  },
  { _id: false }
);

module.exports = shippingAddressSchema;
