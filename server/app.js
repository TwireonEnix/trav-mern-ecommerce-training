require('dotenv').config({ path: '.env.dev' });

const express = require('express');
require('express-async-errors');

const app = express();
const morgan = require('morgan');
const cors = require('cors');

const ProductRoutes = require('./components/Products');
const AuthRoutes = require('./components/Auth');
const UserRoutes = require('./components/User');

const error = require('./middleware/error');

require('./startup/db')();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(cors());
app.use(morgan('dev'));
app.get('/', (req, res) => res.send({ msg: 'Hello World' }));

app.use('/auth', AuthRoutes);
app.use('/product', ProductRoutes);
app.use('/user', UserRoutes);
app.use('/seed', require('./startup/seedDb'));

app.use(error);

module.exports = app;
