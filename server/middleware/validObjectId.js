const { ObjectId } = require('mongoose').Types;

module.exports = (req, res, next) => {
  if (ObjectId.isValid(req.params.id)) return next();
  res.status(400).send({ code: 400, message: 'Invalid Object Id' });
};
