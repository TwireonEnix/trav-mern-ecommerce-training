const R = require('ramda');
const jwt = require('jsonwebtoken');

const verifyJWT = async (req, res, next) => {
  const token = R.path(['headers', 'x-auth'], req);
  if (R.isNil(token)) return res.status(401).send({ message: 'No token provided' });

  try {
    const payload = jwt.verify(token, process.env.JWT_SECRET);
    req.user = payload;
    return next();
  } catch (e) {
    return res.status(400).send({ message: 'Invalid token' });
  }
};

module.exports = verifyJWT;
