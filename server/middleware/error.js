module.exports = (error, {}, res, _) => {
  if (error.status) {
    const { status, message } = error;
    return res.status(status).send({ status, message });
  }
  console.log(error);
  console.log(new Date().toISOString());
  res.status(500).send({ message: 'Something failed!', error: error.message });
};
