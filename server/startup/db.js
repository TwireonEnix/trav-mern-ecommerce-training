const mongoose = require('mongoose')

const connectDB = async _ => {
  const { NODE_ENV, MONGO_URI } = process.env
  try {
    await mongoose.connect(MONGO_URI)
    //   {
    //   useNewUrlParser: true,
    //   useUnifiedTopology: true
    // useCreateIndex: true
    // } ---> No longer required after mongoose v6
    console.log(`Connected to DB in ${NODE_ENV} mode`)
  } catch (e) {
    console.log(e)
    throw new Error(e.message)
  }
}

module.exports = connectDB
