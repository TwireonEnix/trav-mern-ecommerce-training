const R = require('ramda');
const router = require('express').Router();

const { Order } = require('../components/Order/models');
const { Product } = require('../components/Products/product.model');
const { User } = require('../components/User/user.model');

const { products } = require('../components/Products/core/data');
const { users } = require('../components/User/core/data');

const destroyData = () => Promise.all([User.deleteMany(), Product.deleteMany(), Order.deleteMany()]);

const importData = async () => {
  try {
    await destroyData();
    const createdUsers = await User.create(users);

    await R.compose(
      sampleProducts => Product.create(sampleProducts),
      owner => R.map(R.assoc('owner', owner), products),
      R.path([0, '_id'])
    )(createdUsers);

    console.log('data imported');
  } catch (error) {
    console.log(error);
  }
};

router.get('/import', async (req, res) => {
  await importData();
  res.send({ message: 'Data imported' });
});

router.get('/destroy', async (req, res) => {
  await destroyData();
  res.send({ message: 'Data destroyed' });
});

module.exports = router;
