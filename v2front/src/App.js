// Starting in version 17, import react is no longer needed

import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import Header from './components/Header'
import Footer from './components/Footer'
import { Container } from 'react-bootstrap'

import HomeScreen from './screens/Home'
import ProductScreen from './screens/ProductScreen'

/** Main app: functional components with hooks, I need to understand what does hooks do under the hood in a more specialized course */
const App = () => (
  // jsx, where we cannot use class. and we can have javascript expressions embedded in the jsx. Each component can only output a single jsx element
  // an empty element is called a fragment
  <>
    <Router>
      <Header />
      <main className='py-3'>
        <Container>
          <Routes>
            <Route path='/' element={<HomeScreen />} />
            <Route path='/product/:id' element={<ProductScreen />} />
          </Routes>
        </Container>
      </main>
      <Footer />
    </Router>
  </>
)

export default App
