import { Col, Row } from 'react-bootstrap'
import Product from '../components/Product'
import * as R from 'ramda'
import { useEffect, useState } from 'react'
import { callApi } from '../api'

const HomeScreen = () => {
  const [products, setProducts] = useState([])

  // can't call use effect with an async function (React says: useEffect callbacks are synchronous to prevent race conditions)
  useEffect(() => {
    const fetchProducts = async () => {
      const {
        data: { data: productsFromBackend }
      } = await callApi.get('/product')
      setProducts(productsFromBackend)
    }
    fetchProducts()
  }, []) // if this array is not passed the api gets hit really crazy. a lot of times. indefinitely.

  return (
    <>
      <h1>Latest Products</h1>
      <Row>
        {R.addIndex(R.map)(
          (product, id) => (
            <Col sm={12} md={6} lg={4} xl={3} key={id}>
              <Product product={{ ...product, id }} />
            </Col>
          ),
          products
        )}
      </Row>
    </>
  )
}

export default HomeScreen
