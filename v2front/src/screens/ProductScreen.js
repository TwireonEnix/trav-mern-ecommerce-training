import { prop, isEmpty, complement } from 'ramda'
import { useEffect, useState } from 'react'
import { Row, Col, Image, ListGroup, Card, Button } from 'react-bootstrap'
import { Link, useParams } from 'react-router-dom'
import { callApi } from '../api'
import Rating from '../components/Rating'

const ProductScreen = () => {
  const id = prop('id', useParams())
  const [product, setProduct] = useState({})

  useEffect(() => {
    const fetchProduct = async () => {
      const {
        data: { data: productFromBack }
      } = await callApi.get(`/product/${id}`)
      setProduct(productFromBack)
    }
    fetchProduct()
  }, [id])

  return (
    <>
      <Link className='btn btn-light my-3' to='/'>
        Go Back
      </Link>
      <Row>
        <Col md={6}>
          <Image src={prop('image', product)} alt={prop('name', product)} fluid />
        </Col>
        <Col md={3}>
          <ListGroup variant='flush'>
            <ListGroup.Item>
              <h3>{prop('name', product)}</h3>
            </ListGroup.Item>
            <ListGroup.Item>
              {complement(isEmpty)(product) && (
                <Rating value={prop('rating', product)} text={`${prop('numReviews', product)} reviews`} />
              )}
            </ListGroup.Item>
            <ListGroup.Item>Price: ${prop('price', product)}</ListGroup.Item>
            <ListGroup.Item>Description: {prop('description', product)}</ListGroup.Item>
          </ListGroup>
        </Col>
        <Col md={3}>
          <Card>
            <ListGroup variant='flush'>
              <ListGroup.Item>
                <Row>
                  <Col> Price</Col>
                  <Col>
                    <strong>${prop('price', product)}</strong>
                  </Col>
                </Row>
              </ListGroup.Item>
              <ListGroup.Item>
                <Row>
                  <Col> Status </Col>
                  <Col>{prop('countInStock', product) > 0 ? 'In Stock' : 'Unavailable'}</Col>
                </Row>
              </ListGroup.Item>
              <ListGroup.Item>
                <Button className='btn block' type='button' disabled={prop('countInStock') > 0}>
                  Add to Cart
                </Button>
              </ListGroup.Item>
            </ListGroup>
          </Card>
        </Col>
      </Row>
    </>
  )
}

export default ProductScreen
