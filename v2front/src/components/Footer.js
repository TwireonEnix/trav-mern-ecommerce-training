import { Container, Col, Row } from 'react-bootstrap'

const Footer = () => (
  <footer>
    <Container>
      <Row>
        <Col className='text-center py-3'>Copyright &copy; ReactShop</Col>
      </Row>
    </Container>
  </footer>
)

export default Footer
