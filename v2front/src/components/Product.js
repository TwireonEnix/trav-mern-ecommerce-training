import { prop } from 'ramda'
import { Card } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import Rating from './Rating'

const Product = ({ product }) => (
  <>
    <Card className='my-3 p-3 rounded'>
      <Link to={`/product/${prop('id', product)}`}>
        <Card.Img src={prop('image', product)} variant='top' />
      </Link>
      <Card.Title as='div'>
        <strong>{prop('name', product)}</strong>
      </Card.Title>
      <Card.Text as='div'>
        {/* <div className='my-3'>
          {product.rating} from {product.numReviews} reviews
        </div> */}
        <Rating value={prop('rating', product)} text={`${prop('numReviews', product)} reviews`}></Rating>
      </Card.Text>
      <Card.Text as='h3' className='py-3'>
        {' '}
        ${prop('price', product)}
      </Card.Text>
    </Card>
  </>
)

export default Product
