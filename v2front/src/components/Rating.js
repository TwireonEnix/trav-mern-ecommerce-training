import { map, cond, lte, always, T } from 'ramda'
import PropTypes from 'prop-types'

const getStarIcon = r =>
  cond([
    [lte(r), always('fas fa-star')],
    [lte(r - 0.5), always('fas fa-star-half-alt')],
    [T, always('far fa-star')]
  ])

// styles are added in jsx in double curly braces
const getSingleStar = (rating, color) => i =>
  (
    <span key={i}>
      <i style={{ color }} className={getStarIcon(i)(rating)} />
    </span>
  )

const Rating = ({ value, text, color }) => (
  <div className='rating'>
    {map(getSingleStar(value, color), [1, 2, 3, 4, 5])}
    <span> {text && text}</span>
  </div>
)

Rating.defaultProps = {
  color: '#f8e825'
}

Rating.propTypes = {
  value: PropTypes.number.isRequired,
  text: PropTypes.string.isRequired,
  color: PropTypes.string
}

export default Rating
