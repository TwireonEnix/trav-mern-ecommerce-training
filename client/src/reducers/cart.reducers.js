import { CART_ADD_ITEM, CART_REMOVE_ITEM } from '../constants/cart.constants';
import { o, isNil, find, map, prop, when, eqProps, always, append, reject, propEq } from 'ramda';

export const cartReducer = (state = { cartItems: [] }, action) => {
  switch (action.type) {
    case CART_ADD_ITEM:
      const { payload: item } = action;
      const existItem = o(find(eqProps('_id', item)), prop('cartItems'))(state);

      const cartItems = isNil(existItem)
        ? append(item, state.cartItems)
        : map(when(eqProps('_id', item), always(item)), state.cartItems);

      return { ...state, cartItems };

    case CART_REMOVE_ITEM:
      const { payload } = action;
      return { ...state, cartItems: reject(propEq('_id', payload), state.cartItems) };
    default:
      return state;
  }
};
