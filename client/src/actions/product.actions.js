import {
  PRODUCT_LIST_REQUEST,
  PRODUCT_LIST_SUCCESS,
  PRODUCT_LIST_FAIL,
  PRODUCT_DETAILS_REQUEST,
  PRODUCT_DETAILS_SUCCESS,
  PRODUCT_DETAILS_FAIL
} from '../constants/product.constants';
import { ax } from '../api';

export const listProducts = () => async dispatch => {
  try {
    dispatch({ type: PRODUCT_LIST_REQUEST });

    const {
      data: { data }
    } = await ax.get('/product');

    dispatch({ type: PRODUCT_LIST_SUCCESS, payload: data });
  } catch (error) {
    console.log(error);
    dispatch({ type: PRODUCT_LIST_FAIL, payload: error.response });
  }
};

export const productDetails = productId => async dispatch => {
  try {
    dispatch({ type: PRODUCT_DETAILS_REQUEST });

    const {
      data: { data }
    } = await ax.get(`/product/${productId}`);

    dispatch({ type: PRODUCT_DETAILS_SUCCESS, payload: data });
  } catch (error) {
    console.log(error);
    dispatch({ type: PRODUCT_DETAILS_FAIL, payload: error.response });
  }
};
