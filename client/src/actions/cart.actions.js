import { ax } from '../api';
import { CART_ADD_ITEM, CART_REMOVE_ITEM } from '../constants/cart.constants';
import { pick, prop } from 'ramda';

const setLocalStorage = state => {
  const { cartItems } = prop('cart', state());
  localStorage.setItem('cartItems', JSON.stringify(cartItems));
};

export const addToCart = (productId, qty) => async (dispatch, getState) => {
  try {
    const {
      data: { data }
    } = await ax.get(`/product/${productId}`);

    dispatch({
      type: CART_ADD_ITEM,
      payload: {
        ...pick(['_id', 'name', 'image', 'price', 'countInStock'], data),
        qty
      }
    });

    setLocalStorage(getState);
  } catch (e) {
    console.log(e);
  }
};

export const removeItemFromCart = productId => (dispatch, getState) => {
  dispatch({
    type: CART_REMOVE_ITEM,
    payload: productId
  });
  setLocalStorage(getState);
};
