import { pathOr } from 'ramda';
import { ax } from '../api';
import {
  USER_LOGIN_FAIL,
  USER_LOGIN_REQUEST,
  USER_LOGIN_SUCCESS,
  USER_LOGOUT
} from '../constants/user.constants';

export const login = credentials => async dispatch => {
  try {
    dispatch({ type: USER_LOGIN_REQUEST });

    const {
      data: { data }
    } = await ax.post('/auth/login', credentials);
    console.log(data);

    dispatch({ type: USER_LOGIN_SUCCESS, payload: data });

    localStorage.setItem('userInfo', JSON.stringify(data));
  } catch (error) {
    dispatch({
      type: USER_LOGIN_FAIL,
      payload: pathOr('Server Error', ['response', 'data', 'message'], error)
    });
  }
};

export const logout = () => dispatch => {
  localStorage.removeItem('userInfo');
  dispatch({ type: USER_LOGOUT });
};
