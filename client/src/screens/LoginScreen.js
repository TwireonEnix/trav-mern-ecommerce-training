import { compose, isEmpty, last, propOr, prop, split, when } from 'ramda';
import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Form, Button, Row, Col } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import Message from '../components/Message';
import Loader from '../components/Loader';
import FormContainer from '../components/FormContainer';
import { login } from '../actions/user.actions';

const LoginScreen = ({ location, history }) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const dispatch = useDispatch();
  const { loading, error, userInfo } = useSelector(prop('user'));

  const redirect = compose(
    when(isEmpty, _ => '/'),
    last,
    split('='),
    propOr('', 'search')
  )(location);

  useEffect(() => {
    if (userInfo) history.push(redirect);
  }, [history, userInfo, redirect]);

  const submitHandler = e => {
    e.preventDefault();
    //DISPATCH LOGIN
    dispatch(login({ email, password }));
  };

  return (
    <FormContainer>
      <h1>Sign In</h1>
      {error && <Message variant='danger'>{error}</Message>}
      {loading && <Loader />}
      <Form onSubmit={submitHandler}>
        <Form.Group controlId='email'>
          <Form.Label>Email Address</Form.Label>
          <Form.Control
            type='email'
            placeholder='Email'
            value={email}
            onChange={({ target: { value } }) => setEmail(value)}></Form.Control>
        </Form.Group>
        <Form.Group controlId='Password'>
          <Form.Label>Password</Form.Label>
          <Form.Control
            type='password'
            placeholder='Password'
            value={password}
            onChange={({ target: { value } }) => setPassword(value)}></Form.Control>
        </Form.Group>

        <Button type='submit' variant='primary'>
          Sign In
        </Button>
      </Form>

      <Row className='py3'>
        <Col>
          {' '}
          New Customer? <Link to={redirect ? `/register?redirect=${redirect}` : '/redirect'}></Link>
        </Col>
      </Row>
    </FormContainer>
  );
};

export default LoginScreen;
