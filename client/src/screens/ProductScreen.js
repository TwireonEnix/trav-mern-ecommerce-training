import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { Link } from 'react-router-dom';
import { Row, Col, Image, ListGroup, Card, Button, Form } from 'react-bootstrap';
import Rating from '../components/Rating';
import { path, defaultTo, prop, range, o, inc, map } from 'ramda';

import { productDetails } from '../actions/product.actions';
import Loader from '../components/Loader';
import Message from '../components/Message';

const ProductScreen = ({ history, match }) => {
  const [qty, setQty] = useState(1);

  // match is (i assume) where the router info is located when loading this component through the router.
  const id = path(['params', 'id'], match);
  const dispatch = useDispatch();

  const { loading, error, product } = useSelector(prop('productDetails'));

  useEffect(() => {
    dispatch(productDetails(id));
  }, [dispatch, id]);

  const { name, image, rating, numReviews, price, description, countInStock } = defaultTo({}, product);

  const inStock = countInStock > 0;

  const addToCartHandler = () => {
    history.push(`/cart/${id}?qty=${qty}`);
  };

  return (
    <>
      <Link className='btn btn-light my-3' to='/'>
        Go Back
      </Link>
      {loading ? (
        <Loader />
      ) : error ? (
        <Message variant='danger'> {error} </Message>
      ) : (
        <Row>
          <Col md={6}>
            <Image src={image} alt={name} fluid></Image>
          </Col>
          <Col md={3}>
            <ListGroup variant='flush'>
              <ListGroup.Item>
                <h3>{name}</h3>
              </ListGroup.Item>
              <ListGroup.Item>
                <Rating value={rating} text={numReviews}></Rating>
              </ListGroup.Item>
              <ListGroup.Item>Price: ${price}</ListGroup.Item>
              <ListGroup.Item>Description: {description}</ListGroup.Item>
            </ListGroup>
          </Col>
          <Col md={3}>
            <Card>
              <ListGroup variant='flush'>
                <ListGroup.Item>
                  <Row>
                    <Col>Price </Col>
                    <Col>
                      <strong>{price}</strong>
                    </Col>
                  </Row>
                </ListGroup.Item>
                <ListGroup.Item>
                  <Row>
                    <Col>Status: </Col>
                    <Col>{inStock ? 'In Stock' : 'Out of Stock'}</Col>
                  </Row>
                </ListGroup.Item>
                {countInStock > 0 && (
                  <ListGroup.Item>
                    <Row>
                      <Col>Qty</Col>
                      <Col>
                        <Form.Control as='select' value={qty} onChange={e => setQty(e.target.value)}>
                          {o(
                            map(x => (
                              <option key={inc(x)} value={inc(x)}>
                                {inc(x)}
                              </option>
                            )),
                            range(0)
                          )(countInStock)}
                        </Form.Control>
                      </Col>
                    </Row>
                  </ListGroup.Item>
                )}

                <ListGroup.Item>
                  <Button onClick={addToCartHandler} className='btn-block' type='button' disabled={!inStock}>
                    Add To Cart
                  </Button>
                </ListGroup.Item>
              </ListGroup>
            </Card>
          </Col>
        </Row>
      )}
    </>
  );
};
export default ProductScreen;

/** Before REDUX */
// import React, { useState, useEffect } from 'react';
// import { Link } from 'react-router-dom';
// import { Row, Col, Image, ListGroup, Card, Button } from 'react-bootstrap';
// import Rating from '../components/Rating';
// import axios from 'axios';
// import { path, defaultTo } from 'ramda';

// const ProductScreen = ({ match }) => {
//   // match is (i assume) where the router info is located when loading this component through the router.
//   const id = path(['params', 'id'], match);
//   const [product, setProduct] = useState({});

//   useEffect(() => {
//     // useEffect does not receive async fn?
//     const fetchProduct = async () => {
//       const {
//         data: { data }
//       } = await axios.get(`http://localhost:5000/product/${id}`);
//       setProduct(data);
//     };
//     fetchProduct();
//   }, [id]);

//   const { name, image, rating, numReviews, price, description, countInStock } = defaultTo({}, product);

//   const inStock = countInStock > 0;

//   return (
//     <>
//       <Link className='btn btn-light my-3' to='/'>
//         Go Back
//       </Link>
//       <Row>
//         <Col md={6}>
//           <Image src={image} alt={name} fluid></Image>
//         </Col>
//         <Col md={3}>
//           <ListGroup variant='flush'>
//             <ListGroup.Item>
//               <h3>{name}</h3>
//             </ListGroup.Item>
//             <ListGroup.Item>
//               <Rating value={rating} text={numReviews}></Rating>
//             </ListGroup.Item>
//             <ListGroup.Item>Price: ${price}</ListGroup.Item>
//             <ListGroup.Item>Description: {description}</ListGroup.Item>
//           </ListGroup>
//         </Col>
//         <Col md={3}>
//           <Card>
//             <ListGroup variant='flush'>
//               <ListGroup.Item>
//                 <Row>
//                   <Col>Price </Col>
//                   <Col>
//                     <strong>{price}</strong>
//                   </Col>
//                 </Row>
//               </ListGroup.Item>
//               <ListGroup.Item>
//                 <Row>
//                   <Col>Status: </Col>
//                   <Col>{inStock ? 'In Stock' : 'Out of Stock'}</Col>
//                 </Row>
//               </ListGroup.Item>
//               <ListGroup.Item>
//                 <Button className='btn-block' type='button' disabled={!inStock}>
//                   Add To Cart
//                 </Button>
//               </ListGroup.Item>
//             </ListGroup>
//           </Card>
//         </Col>
//       </Row>
//     </>
//   );
// };
// export default ProductScreen;
