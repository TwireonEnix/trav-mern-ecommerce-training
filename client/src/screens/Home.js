import React, { useEffect } from 'react';
import { Row, Col } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { listProducts } from '../actions/product.actions.js';
// import axios from 'axios';

import Product from '../components/Product';
import Message from '../components/Message';
import Loader from '../components/Loader';

import { map, prop } from 'ramda';

const showProducts = product => (
  <Col key={product._id} sm={12} md={6} lg={4} xl={3}>
    <Product product={product} />
  </Col>
);

const HomeScreen = () => {
  // const [products, setProducts] = useState([]); not required due to redux
  const dispatch = useDispatch();

  useEffect(() => {
    // // useEffect does not receive async fn? REMOVED DUE TO REDUX
    // const fetchProducts = async () => {
    //   const {
    //     data: { data }
    //   } = await axios.get('http://localhost:5000/product');
    //   setProducts(data);
    // };
    // fetchProducts();
    dispatch(listProducts());
  }, [dispatch]);

  const { loading, error, products } = useSelector(prop('productList'));

  return (
    <>
      <h1>Latest Products</h1>
      {loading ? (
        <Loader />
      ) : error ? (
        <Message variant='danger'>{error}</Message>
      ) : (
        <Row>{map(showProducts, products)}</Row>
      )}
    </>
  );
};

export default HomeScreen;
