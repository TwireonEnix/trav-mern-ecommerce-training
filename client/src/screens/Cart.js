import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { Row, Col, ListGroup, Image, Form, Button, Card } from 'react-bootstrap';
import Message from '../components/Message';
import { addToCart, removeItemFromCart } from '../actions/cart.actions';
import {
  path,
  prop,
  compose,
  propOr,
  length,
  map,
  inc,
  o,
  range,
  pluck,
  sum,
  reduce,
  multiply,
  add,
  lt
} from 'ramda';
import { parseReactRouterQueryString } from '../utils/';

const inCart = dispatch => ({ _id, image, name, price, countInStock, qty }) => (
  <ListGroup.Item key={_id}>
    <Row>
      <Col md={2}>
        <Image src={image} alt={name} fluid rounded />
      </Col>
      <Col md={4}>
        <Link to={`/product/${_id}`}>{name}</Link>
      </Col>
      <Col md={2}>$ {price}</Col>
      <Col md={2}>
        <Form.Control
          as='select'
          value={qty}
          onChange={e => dispatch(addToCart(_id, Number(e.target.value)))}>
          {o(
            map(x => (
              <option key={inc(x)} value={inc(x)}>
                {inc(x)}
              </option>
            )),
            range(0)
          )(countInStock)}
        </Form.Control>
      </Col>
      <Col md={2}>
        <Button type='button' variant='light' onClick={() => dispatch(removeItemFromCart(_id))}>
          <i className='fas fa-trash'></i>
        </Button>
      </Col>
    </Row>
  </ListGroup.Item>
);

const CartScreen = ({ match, location, history }) => {
  const productId = path(['params', 'id'], match);

  const qty = compose(Number, propOr(1, 'qty'), parseReactRouterQueryString)(location); // query params;

  const dispatch = useDispatch();

  const { cartItems } = useSelector(prop('cart'));

  const hasItems = o(lt(0), length)(cartItems);

  useEffect(() => {
    if (productId) dispatch(addToCart(productId, qty));
  }, [dispatch, productId, qty]);

  const checkoutHandler = () => history.push('/login?redirect=shipping');

  return (
    <Row>
      <Col md={8}>
        <h1>Shopping Cart</h1>
        {hasItems ? (
          <ListGroup variant='flush'>{map(inCart(dispatch), cartItems)}</ListGroup>
        ) : (
          <Message my={2}>
            Cart is empty. <Link to='/'>Go Back</Link>
          </Message>
        )}
      </Col>
      <Col md={4}>
        <Card>
          <ListGroup variant='flush'>
            <ListGroup.Item>
              <h2>Subtotal ({compose(sum, pluck('qty'))(cartItems)}) items</h2>$
              {reduce((a, { qty, price }) => compose(add(a), multiply(qty))(price), 0, cartItems).toFixed(2)}
            </ListGroup.Item>
            <ListGroup.Item>
              <Button type='button' className='btn-block' disabled={!hasItems} onClick={checkoutHandler}>
                Proceed to Checkout
              </Button>
            </ListGroup.Item>
          </ListGroup>
        </Card>
      </Col>
    </Row>
  );
};

export default CartScreen;
