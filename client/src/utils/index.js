import { prop, tail, compose, split, map, fromPairs } from 'ramda';

export const parseReactRouterQueryString = compose(
  fromPairs,
  map(split('=')),
  split('&'),
  tail,
  prop('search')
);
