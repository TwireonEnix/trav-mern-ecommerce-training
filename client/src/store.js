import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import { always, compose, ifElse, isNil } from 'ramda';

import { productReducer, productDetailsReducer } from './reducers/product.reducers';
import { cartReducer } from './reducers/cart.reducers';
import { userLoginReducer } from './reducers/user.reducer';

const reducer = combineReducers({
  productList: productReducer,
  productDetails: productDetailsReducer,
  cart: cartReducer,
  user: userLoginReducer
});

const retrieveFromLocalStorageOrDefault = (itemLabel, dft) =>
  compose(
    ifElse(isNil, always(dft), v => JSON.parse(v)),
    p => localStorage.getItem(p)
  )(itemLabel);

const cartItems = retrieveFromLocalStorageOrDefault('cartItems', []);
const userInfoFromStorage = retrieveFromLocalStorageOrDefault('userInfo', undefined);

const initialState = { cart: { cartItems }, user: { userInfo: userInfoFromStorage } };

const middleware = [thunk];

const store = createStore(reducer, initialState, composeWithDevTools(applyMiddleware(...middleware)));

export default store;
