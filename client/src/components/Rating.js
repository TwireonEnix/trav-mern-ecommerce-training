import React from 'react';
import propTypes from 'prop-types';
import { cond, lte, always, T, map } from 'ramda';

const { number, string } = propTypes;

const getStarIcon = r =>
  cond([
    [lte(r), always('fas fa-star')],
    [lte(r - 0.5), always('fas fa-star-half-alt')],
    [T, always('far fa-star')]
  ]);

const getSingleStar = (rating, color) => ind => (
  <span key={ind}>
    <i style={{ color }} className={getStarIcon(ind)(rating)}></i>
  </span>
);

const Rating = ({ value, text, color }) => {
  return (
    <div className='rating'>
      {map(getSingleStar(value, color), [1, 2, 3, 4, 5])}
      <span> {text && text} reviews</span>
    </div>
  );
};

/** React prop enforcements or defaults */
Rating.defaultProps = {
  color: '#f8e825',
  value: 0,
  text: 0
};

Rating.propTypes = {
  // value: number.isRequired,
  value: number,
  text: number,
  color: string
};

export default Rating;
