import dotenv from 'dotenv'
import fastify from 'fastify'
import cors from '@fastify/cors'
import helmet from '@fastify/helmet'
import pipeline from './init/pipeline.js'
import connectDB from './init/db.js'
import errorHandler from './middleware/errorHandler.js'

dotenv.config({ path: '.env' })
await connectDB()

const app = fastify({ logger: false })

app.register(cors)
app.register(helmet)

app.get('/', async (req, reply) => reply.send({ test: 'Hello friend' }))

pipeline(app)

app.setErrorHandler(errorHandler)

await app.ready()

export default app
