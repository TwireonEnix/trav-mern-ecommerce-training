import { productRoutes } from '../components/Product/index.js'
import dataSeeders from '../db-seeders/seedDb.js'

export default async function (app) {
  await app.register(productRoutes, { prefix: '/product' })
  await app.register(dataSeeders, { prefix: '/data' })
}
