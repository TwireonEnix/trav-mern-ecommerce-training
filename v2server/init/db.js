import { connect } from 'mongoose'

const connectDB = async () => {
  const { NODE_ENV, MONGO_URI } = process.env
  try {
    await connect(MONGO_URI, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    })
    if (NODE_ENV !== 'test') console.log(`Connected to mongodb in ${NODE_ENV} mode.`)
  } catch (error) {
    console.log(error)
    process.exit(1)
  }
}
export default connectDB
