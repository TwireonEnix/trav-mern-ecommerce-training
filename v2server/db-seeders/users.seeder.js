import bcrypt from 'bcryptjs'

const getHashSync = p => bcrypt.hashSync(p, 10)

export default [
  {
    name: 'Admin User',
    email: 'admin@example.com',
    password: getHashSync('adminPassword'),
    isAdmin: true
  },
  {
    name: 'John Doe',
    email: 'john@example.com',
    password: getHashSync('johnPassword'),
    isAdmin: false
  },
  {
    name: 'Jane Doe',
    email: 'jane@example.com',
    password: getHashSync('janePassword'),
    isAdmin: false
  }
]
