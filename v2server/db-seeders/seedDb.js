import * as R from 'ramda'

import { Product } from '../components/Product/models/index.js'
import { User } from '../components/User/models/index.js'
import { Order } from '../components/Order/models/index.js'

import products from './products.seeder.js'
import users from './users.seeder.js'

const destroyData = () => Promise.all([Product.deleteMany(), User.deleteMany(), Order.deleteMany()])

const importData = async () => {
  await destroyData()

  const createdUsers = await User.create(users)

  await R.compose(
    sampleProducts => Product.create(sampleProducts),
    uploader => R.map(R.mergeLeft({ uploader }), products),
    R.path([0, '_id'])
  )(createdUsers)
}

export default async function (f) {
  f.get('/import', async (req, reply) => {
    await importData()
    reply.send({ message: 'Data imported' })
  })

  f.get('destroy', async (req, reply) => {
    await destroyData()
    reply.send({ message: 'Data destroyed' })
  })
}
