import app from './app.js'

const { PORT, NODE_ENV } = process.env

const start = async () => {
  try {
    await app.listen({ port: PORT })
    console.log('Fastify server running on port:', PORT)
  } catch (e) {
    app.log.error(e)
    process.exit(1)
  }
}

start()
