async function errorHandler(error, req, reply) {
  if (error.statusCode) {
    const { statusCode, message } = error
    return reply.status(statusCode).send({ statusCode: statusCode, message })
  }

  return reply
    .status(500)
    .send({ statusCode: 500, error: 'Internal Server Error', stack: error.stack, message: error.message })
}

export default errorHandler
