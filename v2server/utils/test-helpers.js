import app from '../app.js'
import supertest from 'supertest'

const errorAssertion = () => {}

const callApp = supertest(app.server)

export { callApp, errorAssertion }
