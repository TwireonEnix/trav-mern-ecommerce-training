const errorResponse = (statusCode, message) => ({
  error: true,
  statusCode,
  message
})

export { errorResponse }
