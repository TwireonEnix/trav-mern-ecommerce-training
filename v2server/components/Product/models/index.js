import { Schema, model, Types } from 'mongoose'
import { reviewSchema } from './review.js'

const productSchema = new Schema(
  {
    uploader: { type: Types.ObjectId, required: true, ref: 'user' },
    name: { type: String, required: true },
    image: { type: String, required: true },
    brand: { type: String, required: true },
    category: { type: String, required: true },
    description: { type: String, required: true },
    reviews: { type: [reviewSchema], required: true },
    rating: { type: Number, required: true, default: 0 },
    countReviews: { type: Number, required: true, default: 0 },
    price: { type: Number, required: true },
    countInStock: { type: Number, required: true }
  },
  { timestamps: true }
)

const Product = model('product', productSchema)

export { Product }
