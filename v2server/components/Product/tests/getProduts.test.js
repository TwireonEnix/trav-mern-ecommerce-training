import { assert } from 'chai'
import { callApp } from '../../../utils/test-helpers.js'

const exec = () => callApp.get('/product')

describe('We will run a simple test', () => {
  it('should respond at least', async () => {
    const { status, body } = await exec()
    assert.strictEqual(status, 200)
    console.log(body)
  })
})
