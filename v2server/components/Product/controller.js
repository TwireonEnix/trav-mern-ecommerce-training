import { retrieveAllProducts, retrieveProductById } from './core/product.crud.js'

/** @desc: Fetches all the products
 * @route: GET /product
 * @access: Public
 */
const getAllProducts = async (req, reply, next) => {
  const data = await retrieveAllProducts()
  return reply.send({ code: 200, message: 'Products Retrieved', data })
}

/** @desc: Fetches a single product
 * @route: GET /product/:id
 * @access: Public
 */
const getSingleProduct = async ({ params, user }, reply, next) => {
  const { id } = params
  const data = await retrieveProductById(id)
  return reply.send({ code: 200, message: 'Product found', data: { ...data, user } })
}

export { getAllProducts, getSingleProduct }
