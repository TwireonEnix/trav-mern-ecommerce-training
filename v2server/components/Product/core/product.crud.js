// No longer needed after implementing database calls
// import { products } from './products.js'
// const productsInMemoryWithId = R.addIndex(R.map)((p, _id) => ({ ...p, _id }), products)
// const retrieveProductById = _id => R.find(R.whereEq({ _id }), productsInMemoryWithId)

import * as R from 'ramda'
import { Product } from '../models/index.js'
import { errorResponse } from '../../../utils/index.js'

const retrieveAllProducts = () => Product.find().lean().exec()

const retrieveProductById = R.compose(
  R.andThen(
    R.when(R.isNil, () => {
      throw errorResponse(404, 'Product not found')
    })
  ),
  _id => Product.findById(_id).lean().exec()
)

export { retrieveAllProducts, retrieveProductById }
