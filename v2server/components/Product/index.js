import { getAllProducts, getSingleProduct } from './controller.js'
import { verifyUser } from '../../middleware/verifyJWT.js'
import chain from '../../middleware/onChain.js'

const productRoutes = async fastify => {
  fastify.get('/:id', { preHandler: [verifyUser, chain] }, getSingleProduct)
  fastify.get('/', getAllProducts)
}

export { productRoutes }
