import { Schema, Types } from 'mongoose'

const orderItemSchema = new Schema(
  {
    name: { type: String, required: true },
    quantity: { type: Number, required: true },
    image: { type: String, required: true },
    price: { type: Number, required: true },
    product: { type: Types.ObjectId, required: true, ref: 'product' }
  },
  { _id: false }
)

export { orderItemSchema }
