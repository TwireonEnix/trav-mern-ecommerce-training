import { Schema } from 'mongoose'

const addressSchema = new Schema(
  {
    address: { type: String, required: true },
    city: { type: Number, required: true },
    zipCode: { type: String, required: true },
    country: { type: Number, required: true }
  },
  { _id: false }
)

export { addressSchema }
