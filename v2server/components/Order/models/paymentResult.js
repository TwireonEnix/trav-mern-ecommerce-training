import mongoose from 'mongoose'

const paymentResultSchema = new mongoose.Schema(
  {
    id: { type: String, required: true },
    status: { type: Number, required: true },
    update_time: { type: String, required: true },
    email_address: { type: Number, required: true }
  },
  { _id: false }
)

export { paymentResultSchema }
