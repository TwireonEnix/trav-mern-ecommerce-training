import { Schema, Types, model } from 'mongoose'
import { orderItemSchema } from './item.js'
import { addressSchema } from './address.js'
import { paymentResultSchema } from './paymentResult.js'

const orderSchema = new Schema(
  {
    buyer: { type: Types.ObjectId, required: true, ref: 'user' },
    orderItems: { type: [orderItemSchema], required: true, default: [] },
    shippingAddress: { type: addressSchema, required: true },
    paymentMethod: { type: String, required: true },
    paymentResult: { type: paymentResultSchema },
    taxPrice: { type: Number, required: true, default: 0.0 },
    shippingPrice: { type: Number, required: true, default: 0.0 },
    totalPrice: { type: Number, required: true, default: 0.0 },
    isPaid: { type: Boolean, required: true, default: false },
    paidAt: { type: Date },
    isDelivered: { type: Boolean },
    deliveredAt: { type: Date }
  },
  { timestamps: true }
)

const Order = model('order', orderSchema)

export { Order }
